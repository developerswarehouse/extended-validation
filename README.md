# Extended Validation Rules

A simple package for adding additional regex matches to the Validator.

## Installation

    composer require developerswarehouse/extended-validation

## Usage

The package will load the validation rules from the config file, then extend Laravel's Validator with the key given, as the new validation rule.  You will be able to use the new rules in the same way as the built in validations that come with Laravel.

### Request Validation [(docs)](https://laravel.com/docs/5.5/validation#quick-writing-the-validation-logic)

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'code' => 'required|alpha_dot_alpha',
            'key' => 'required|alpha_dot_alpha_num',
            'token' => 'required|alpha_num_underscore_alpha_num',
        ]);
    }

### Form Requests [(docs)](https://laravel.com/docs/5.5/validation#authorizing-form-requests)

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'code' => 'required|alpha_dot_alpha',
            'key' => 'required|alpha_dot_alpha_num',
            'token' => 'required|alpha_num_underscore_alpha_num',
        ];
    }


## Publish

You are not required to publish the package, but if you wish to extend/alter the rules config, then you will need to.  

     php artisan vendor:publish

### Add Your Validation

In `config/extended-validation.php` you will see a list of rules like:

    return [
        'rules' => [
            "alpha_dot" => "/^[\pL\pM\pN.]+$/i",
            "alpha_dot_alpha" => "/^[a-z]+\.[a-z]+$/i",
            "alpha_dot_num" => "/^[a-z]+\.[0-9]+$/i",
            "num_dot_alpha" => "/^\d+\.[a-z]+$/i",

            // ...

        ]
    ];

Feel free to add, remove or modify the rules to better suit your needs.  If you are not familiar with regular expressions, I would suggest you test your rules using an [Online Regex Tester](https://www.google.co.uk/search?q=online+regex+generator).
