<?php

/**
 * 
 */
return [
    'rules' => [
        "alpha_dot" => "/^[\pL\pM\pN.]+$/i",
        "alpha_dot_alpha" => "/^[a-z]+\.[a-z]+$/i",
        "alpha_dot_num" => "/^[a-z]+\.[0-9]+$/i",
        "num_dot_alpha" => "/^\d+\.[a-z]+$/i",
        "num_dot_num" => "/^\d+\.\d+$/i",
        "alpha_num_dot_alpha" => "/^(([a-z0-9])+)\.[a-z]+$/i",
        "alpha_num_dot_num" => "/^(([a-z0-9])+)\.\d+$/i",
        "alpha_dot_alpha_num" => "/[\pL]+\.[\pL\d]+/",
        "alpha_dot_alpha_num_strict" => "/(?=.*(\pL)(\d))(^[\pL]+\.[\pL\d]+$)/",
        "num_dot_alpha_num" => "/^\d\.(([a-z0-9])+)$/i",
        "alpha_dash_alpha" => "/^[a-z]+-[a-z]+$/i",
        "alpha_dash_num" => "/^[a-z]+-[0-9]+$/i",
        "num_dash_alpha" => "/^\d+-[a-z]+$/i",
        "num_dash_num" => "/^\d+-\d+$/i",
        "alpha_num_dash_alpha" => "/^(([a-z0-9])+)-[a-z]+$/i",
        "alpha_num_dash_num" => "/^(([a-z0-9])+)-\d+$/i",
        "alpha_dash_alpha_num" => "/^[a-z]+-(([a-z0-9])+)$/i",
        "num_dash_alpha_num" => "/^\d-(([a-z0-9])+)$/i",
        "alpha_underscore_alpha" => "/^[a-z]+_[a-z]+$/i",
        "alpha_underscore_num" => "/^[a-z]+_[0-9]+$/i",
        "num_underscore_alpha" => "/^\d+_[a-z]+$/i",
        "num_underscore_num" => "/^\d+_\d+$/i",
        "alpha_num_underscore_alpha" => "/^(([a-z0-9])+)_[a-z]+$/i",
        "alpha_num_underscore_num" => "/^(([a-z0-9])+)_\d+$/i",
        "alpha_underscore_alpha_num" => "/^[a-z]+_(([a-z0-9])+)$/i",
        "num_underscore_alpha_num" => "/^\d_(([a-z0-9])+)$/i",
    ],
];
