<?php

namespace DevelopersWarehouse\ExtendedValidation;

use InvalidArgumentException;

class Rules {

    function __call($method, $args)
    {
        // Get the regex pattern, no need to check it exist as content was loaded from config.
        $regex = config('extendedvalidation.rules')[$method];
        return preg_match($regex, $args[1]);
    }

}
