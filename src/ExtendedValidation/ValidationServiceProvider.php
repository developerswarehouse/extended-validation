<?php

namespace DevelopersWarehouse\ExtendedValidation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('extendedvalidation.php'),
        ]);

        // Load the validation rules into the validator
        $rules = (array)config('extendedvalidation.rules', []);
        foreach($rules as $rule => $regex) {
            Validator::extend($rule, 'DevelopersWarehouse\ExtendedValidation\Rules@'.$rule);
        }

    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'extendedvalidation'
        );
    }
}
